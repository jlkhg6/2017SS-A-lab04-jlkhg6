#!/bin/bash

if [[(-n "$1") && (-n "$2")]]; then
  wget -q -O- $2 | grep -o $1 | wc -l
fi
