#!/bin/bash


for args in *; do
   echo $args
   echo "1)View " $args
   echo "2)Edit " $args
   echo "3)Compile "$args
   echo "4)Quit" $args
   read INPUT
   
   case $INPUT in
      
      1)
        cat $args
        ;;
      2)
        jpico $args
        ;;
      3)
        g++ $args -o
        ;;
      4)
        exit
        ;;
      *)
        echo "No file selected "
        ;;
    esac
done 
